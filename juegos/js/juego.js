/******* Cronometro *****/
			var elcrono;
			var emp=undefined;
			var iniciado = false;
			var ronda = 1;
			var tiempos = [];
			function tiempo(){
				nuevafecha=new Date();
				emp = emp==undefined?new Date():emp;
			    cro=nuevafecha-emp; 
			    cr=new Date(); //pasamos el num. de milisegundos a objeto fecha.
			    cr.setTime(cro); 
			    //obtener los distintos formatos de la fecha:
			    cs=cr.getMilliseconds(); //milisegundos 
			    cs=cs/10; //paso a centésimas de segundo.
			    cs=Math.round(cs); //redondear las centésimas
			    sg=cr.getSeconds(); //segundos 
			    mn=cr.getMinutes(); //minutos 
			    ho=cr.getHours()-1; //horas 
			        //poner siempre 2 cifras en los números		 
			    if (cs<10) {cs="0"+cs;} 
			    if (sg<10) {sg="0"+sg;} 
			    if (mn<10) {mn="0"+mn;} 
			       //llevar resultado al visor.					    
			    $(".minutos").html(mn);
			    $(".segundos").html(sg);
			}

			/******* /Cronometro ****/
			/******* Logica juego *****/
			var numeros = ['1','2','3','4','5'];
			var letras = ['A','B','C','D','E'];
			var romanos = ['I','II','III','IV','V'];
			var posiciones = [
				{top:10,left:60},
				{top:22,left:56},
				{top:-1,left:57},
				{top:0,left:42},
				{top:28,left:43},
				{top:11,left:40},
				{top:29,left:63},
				{top:-1,left:25},
				{top:11,left:23},
				{top:29,left:0},
				{top:22,left:38},
				{top:25,left:20},
				{top:12,left:3},
				{top:27,left:7},
				{top:-1,left:6}
			];
			posiciones = posiciones.sort(function() {return Math.random() - 0.5});
			var numeroscount = 0;
			var letrascount = 0;
			var romanoscount = 0;
			var actual = 'n';
			var tam = '';
			$(document).on('ready',function(){
				iniciar();				
			});

			function play(){
				$("#mensajes").fadeOut(500);
			}

			function llenarFields(){				
				var str = '';
				var desorden = '';
				//alert(window.innerWidth+' '+window.innerHeight);
				tam = 5; //Variable
				if(window.innerWidth>=640 && window.innerHeight>=280){
					tam = 6;
				}if(window.innerWidth>=736 && window.innerHeight>=414){
					tam = 5;
				}if(window.innerWidth>=768 && window.innerHeight>=320){
					tam = 6.2;
				}if(window.innerWidth>=1024 && window.innerHeight>=768){
					tam = 4.5;
				}if(window.innerWidth>=1280 && window.innerHeight>=720){
					tam = 5.3;
				}if(window.innerWidth>=1280 && window.innerHeight>=768){
					tam = 4.5;
				}if(window.innerWidth>=1400){
					tam = 6;
				}
				
				var posicion = 0,top,left;
				for(var i=0;i<letras.length;i++){					
					str+= '<li id="l'+i+'" style="top:'+(i*tam)+'vh; z-index:1'+(letras.length-i)+'"></li>';
					top = posiciones[posicion].top+'vh';
					left = posiciones[posicion].left+'vw';
					posicion++;
					desorden+= '<li id="ll'+i+'" data-val="'+letras[i]+'" style="top:'+top+';left:'+left+'"><img src="img/'+letras[i].toLowerCase()+'.svg"></li>';
				}
				$("#cajonl").html(str);
				str = '';
				for(var i=0;i<numeros.length;i++){
					str+= '<li id="n'+i+'" style="top:'+(i*tam)+'vh; z-index:2'+(letras.length-i)+'"></li>';
					top = posiciones[posicion].top+'vh';
					left = posiciones[posicion].left+'vw';
					posicion++;
					desorden+= '<li id="nn'+i+'" data-val="'+numeros[i]+'" style="top:'+top+';left:'+left+'"><img src="img/'+numeros[i]+'.svg"></li>';
				}
				$("#cajonn").html(str);
				str = '';
				for(var i=0;i<romanos.length;i++){
					str+= '<li id="r'+i+'" style="top:'+(i*tam)+'vh; z-index:3'+(letras.length-i)+'"></li>';
					top = posiciones[posicion].top+'vh';
					left = posiciones[posicion].left+'vw';
					posicion++;
					desorden+= '<li id="rr'+i+'" data-val="'+romanos[i]+'" style="top:'+top+';left:'+left+'"><img src="img/'+romanos[i]+'.svg"></li>';
				}
				$("#cajonr").html(str);
				$("#desorden").html(desorden);
			}

			function iniciar(){
				llenarFields();				
				$("#n0").addClass('active');				
				$(document).on('click',"#desorden li",function(){
					if(!iniciado){
						elcrono=setInterval(tiempo,10);
						iniciado = true;
						$("#mensajes").fadeOut(500);
						$(".reloj").html('<span class="minutos">00</span>:<span class="segundos">00</span>');						
						emp=undefined;						
					}
					if(ronda==1){
						logica2($(this));
					}else{
						logica($(this));
					}							
				});
				moverCajon(numeroscount,'n');				
			}

			function moverCajon(contador,elemento){
				var item = $("#"+elemento+contador);
				if(contador==0){
					var newtop = tam;
				}else{
					var newtop = tam+tam*2*contador;
				}								
				item.animate({'top':newtop+'vh'},400);				
			}

			function ponerHoja(elemento,hoja){
				elemento.after('<li class="hoja" style="top:'+elemento.css('top')+'">'+hoja.html()+'</li>');
			}

			function logica(e){

				switch(actual){
					case 'n':
						if(e.data('val')==numeros[numeroscount]){							
							ponerHoja($("#n"+numeroscount),e);
							numeroscount++;
							moverCajon(numeroscount,'n');							
							if(numeroscount==numeros.length){								
								moverCajon(letrascount,'l');
								actual = 'l';
								tiempos[3] = {'minutos':$("#reloj1 .minutos").html(),'segundos':$("#reloj1 .segundos").html()};
								$("#reloj1 .minutos").removeClass('minutos');
								$("#reloj1 .segundos").removeClass('segundos');								
							}
						}
					
					break;
					case 'l':
						if(e.data('val')==letras[letrascount]){						
							ponerHoja($("#l"+letrascount),e);					
							letrascount++;
							moverCajon(letrascount,'l');
							if(letrascount==letras.length){
								moverCajon(romanoscount,'r');
								actual = 'r';	
								tiempos[4] = {'minutos':$("#reloj2 .minutos").html(),'segundos':$("#reloj2 .segundos").html()};
								$("#reloj2 .minutos").removeClass('minutos');
								$("#reloj2 .segundos").removeClass('segundos');
							}					
						}
					break;
					case 'r':
						if(e.data('val')==romanos[romanoscount]){							
							ponerHoja($("#r"+romanoscount),e);
							romanoscount++;
							moverCajon(romanoscount,'r');
							if(romanoscount==romanos.length){
								actual = 'n';
								tiempos[5] = {'minutos':$("#reloj3 .minutos").html(),'segundos':$("#reloj3 .segundos").html()};	
								$("#reloj3 .minutos").removeClass('minutos');
								$("#reloj3 .segundos").removeClass('segundos');
								$("#mensajes").html('<p>Ahora te muestro las diferencias</p>');
								$("#mensajes").append('<p>Forma 1 columna 1: '+tiempos[0].minutos+':'+tiempos[0].segundos+' Forma 2 Columna 1: '+tiempos[3].minutos+':'+tiempos[3].segundos+' </p>');
								$("#mensajes").append('<p>Forma 1 columna 2: '+tiempos[1].minutos+':'+tiempos[1].segundos+' Forma 2 Columna 2: '+tiempos[4].minutos+':'+tiempos[4].segundos+' </p>');
								$("#mensajes").append('<p>Forma 1 columna 3: '+tiempos[2].minutos+':'+tiempos[2].segundos+' Forma 2 Columna 3: '+tiempos[5].minutos+':'+tiempos[5].segundos+' </p>');
								$("#mensajes").fadeIn(500);
							}	
						}
					break;
				}
			}

			function logica2(e){				
				switch(actual){
					case 'n':
						if(e.data('val')==numeros[numeroscount]){							
							ponerHoja($("#n"+numeroscount),e);
							numeroscount++;
							moverCajon(letrascount,'l');
							actual = 'l';

							if(numeroscount==numeros.length){																
								tiempos[0] = {'minutos':$("#reloj1 .minutos").html(),'segundos':$("#reloj1 .segundos").html()};
								$("#reloj1 .minutos").removeClass('minutos');
								$("#reloj1 .segundos").removeClass('segundos');								
							}
						}
					
					break;
					case 'l':
						if(e.data('val')==letras[letrascount]){
							ponerHoja($("#l"+letrascount),e);					
							letrascount++;
							moverCajon(romanoscount,'r');
							actual = 'r';	

							if(letrascount==letras.length){																
								tiempos[1] = {'minutos':$("#reloj2 .minutos").html(),'segundos':$("#reloj2 .segundos").html()};
								$("#reloj2 .minutos").removeClass('minutos');
								$("#reloj2 .segundos").removeClass('segundos');								
							}					
						}
					break;
					case 'r':
						if(e.data('val')==romanos[romanoscount]){
							ponerHoja($("#r"+romanoscount),e);
							romanoscount++;
							moverCajon(numeroscount,'n');
							actual = 'n';	

							if(romanoscount==romanos.length){																
								tiempos[2] = {'minutos':$("#reloj3 .minutos").html(),'segundos':$("#reloj3 .segundos").html()};
								$("#reloj3 .minutos").removeClass('minutos');
								$("#reloj3 .segundos").removeClass('segundos');		
								ronda = 2;
								iniciado = false;
								$("#mensajes").fadeIn(500).html('Muy bien!!!, Ahora intenta realizar el llenado de forma consecutiva por archivero, para ver la diferencia');								
								$("#mensajes").append('<p><a href="javascript:play()">Cerrar</a>');
								clearInterval(elcrono);
								llenarFields();				
								actual = 'n';
								numeroscount = 0;
								letrascount = 0;
								romanoscount = 0;				
								moverCajon(numeroscount,'n');

							}					
						}
					break;
				}
			}