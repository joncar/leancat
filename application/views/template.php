<!doctype html>
<html lang="en">
	<!-- Google Web Fonts
	================================================== -->
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i%7CFrank+Ruhl+Libre:300,400,500,700,900" rel="stylesheet">
	<!-- Basic Page Needs
	================================================== -->
	<title><?= empty($title) ? 'Monalco' : $title ?></title>
	<meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
	<meta name="description" content="<?= empty($keywords) ?'': $description ?>" />
	<link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>
	<link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>
	<link href="<?= base_url() ?>js/stocookie/stoCookie.css" rel="stylesheet">
	<script>var URL = '<?= base_url() ?>';</script>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	
	<!-- CSS -->
	<?php
	if(!empty($css_files)):
	foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?= $file ?>" />
	<?php endforeach; ?>
	<?php endif; ?>
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/rs-plugin/css/settings.min.css" media="screen" />
	<link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/bootstrap.min.css">
	<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700%7COpen+Sans:400,300,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/icons-fonts.css" >
	<link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/style.css" >
	<link rel='stylesheet' href="<?= base_url() ?>theme/theme/css/animate.min.css">
	<!-- IE Warning CSS -->
	<!--[if lte IE 8]><link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/css/ie-warning.css" ><![endif]-->
	<!--[if lte IE 8]><link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/theme/css/ie8-fix.css" ><![endif]-->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<!-- Modernizr -->
	<!-- <script src="js/modernizr.js"></script> -->
</head>
<body>
	<a href="whatsapp://send?text=Hola Mundo&phone=+34609306713" style="font-size:18px;padding:8px 8px;text-decoration:none;background-color:#189D0E;color:white;text-shadow:none;position:fixed;right:0;bottom:0px;z-index: 1000;"><i class="fa fa-whatsapp"></i> whatsapp</a>
	<!-- LOADER -->
	<div id="loader-overflow">
		<div id="loader3">Please enable JS</div>
	</div>
	
	<div id="wrap" class="boxed ">
		<div class="grey-bg"> <!-- Grey BG  -->
		<!--[if lte IE 8]>
		<div id="ie-container">
						<div id="ie-cont-close">
											<a href='#' onclick='javascript&#058;this.parentNode.parentNode.style.display="none"; return false;'><img src='images/ie-warn/ie-warning-close.jpg' style='border: none;' alt='Close'></a>
						</div>
						<div id="ie-cont-content" >
											<div id="ie-cont-warning">
																<img src='images/ie-warn/ie-warning.jpg' alt='Warning!'>
											</div>
											<div id="ie-cont-text" >
																<div id="ie-text-bold">
																					You are using an outdated browser
																</div>
																<div id="ie-text">
																					For a better experience using this site, please upgrade to a modern web browser.
																</div>
											</div>
											<div id="ie-cont-brows" >
																<a href='http://www.firefox.com' target='_blank'><img src='images/ie-warn/ie-warning-firefox.jpg' alt='Download Firefox'></a>
																<a href='http://www.opera.com/download/' target='_blank'><img src='images/ie-warn/ie-warning-opera.jpg' alt='Download Opera'></a>
																<a href='http://www.apple.com/safari/download/' target='_blank'><img src='images/ie-warn/ie-warning-safari.jpg' alt='Download Safari'></a>
																<a href='http://www.google.com/chrome' target='_blank'><img src='images/ie-warn/ie-warning-chrome.jpg' alt='Download Google Chrome'></a>
											</div>
						</div>
		</div>
		<![endif]-->
		<?php
			if(empty($editor)){
				$this->load->view($view);
			}else{
				echo $view;
			}
		?>
		<!-- BACK TO TOP -->
		<p id="back-top">
			<a href="#top" title="Back to Top"><span class="icon icon-arrows-up"></span></a>
		</p>
		</div><!-- End BG -->
		</div><!-- End wrap -->
		
		<?php $this->load->view('includes/template/scripts') ?>
	</body>
</html>