<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/jquery-1.11.2.min.js"></script>
<script src="<?= base_url() ?>theme/theme/js/bootstrap.min.js"></script>
<script src='<?= base_url() ?>theme/theme/js/jquery.magnific-popup.min.js'></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/jquery.countTo.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/jquery.appear.js"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyDzf6Gmc9u7rr2JHijOERAmC_j0gWYtR2c"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/gmap3.min.js"></script>
<!--[if lt IE 10]><script type="text/javascript" src="<?= base_url() ?>theme/theme/js/jquery.placeholder.js"></script><![endif]-->
<script src="<?= base_url() ?>theme/theme/js/jquery.validate.min.js"></script>
<script src="<?= base_url() ?>theme/theme/js/contact-form-validation.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/owl.carousel.min.js"></script>

<!-- MAIN SCRIPT -->
<script src="<?= base_url() ?>theme/theme/js/main.js"></script>

<!-- REVOSLIDER SCRIPTS  -->
<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<script type="text/javascript" src="<?= base_url() ?>theme/theme/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/rs-plugin/js/jquery.themepunch.revolution-parallax.min.js"></script>

<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/jquery.backgroundvideo.min.js"></script> 
<script>
$(document).ready(function(){
  if (!($("html").hasClass("mobile"))){
    var videobackground = new $.backgroundVideo($('.sm-video'), {
      "align": "centerXY",
      "width": 1920,
      "height": 1080,
      "path": "<?= base_url() ?>theme/theme/video/",
      "filename": "The-Crosswalk",
      "types": ["mp4", "webm"],
      "autoplay": true,
      "loop": true
    });
  }
});
</script>


<!-- SLIDER REVOLUTION INIT  -->
<script type="text/javascript">
jQuery(document).ready(function() {
if ( (navigator.appVersion.indexOf("Win")!=-1) && ( ieDetect == false ) ) {
jQuery('#rs-fullwidth').revolution(
{
dottedOverlay:"none",
delay:16000,
startwidth:1170,
startheight:700,
hideThumbs:200,

thumbWidth:100,
thumbHeight:50,
thumbAmount:5,

//fullScreenAlignForce: "off",

navigationType:"none",
navigationArrows:"solo",
navigationStyle:"preview0",

hideTimerBar:"on",

touchenabled:"on",
onHoverStop:"on",

swipe_velocity: 0.7,
swipe_min_touches: 1,
swipe_max_touches: 1,
drag_block_vertical: false,

parallax:"scroll",
parallaxBgFreeze:"on",
parallaxLevels:[45,40,35,50],
parallaxDisableOnMobile:"on",

keyboardNavigation:"off",

navigationHAlign:"center",
navigationVAlign:"bottom",
navigationHOffset:0,
navigationVOffset:20,
soloArrowLeftHalign:"left",
soloArrowLeftValign:"center",
soloArrowLeftHOffset:20,
soloArrowLeftVOffset:0,
soloArrowRightHalign:"right",
soloArrowRightValign:"center",
soloArrowRightHOffset:20,
soloArrowRightVOffset:0,

shadow:0,
fullWidth:"off",
fullScreen:"on",
spinner:"spinner4",

stopLoop:"off",
stopAfterLoops:-1,
stopAtSlide:-1,
shuffle:"off",

autoHeight:"off",
forceFullWidth:"off",

hideThumbsOnMobile:"off",
hideNavDelayOnMobile:1500,
hideBulletsOnMobile:"off",
hideArrowsOnMobile:"off",
hideThumbsUnderResolution:0,

hideSliderAtLimit:0,
hideCaptionAtLimit:0,
hideAllCaptionAtLilmit:0,
startWithSlide:0,
//fullScreenOffsetContainer: ""
});
} else {
jQuery('#rs-fullwidth').revolution(
{
dottedOverlay:"none",
delay:16000,
startwidth:1170,
startheight:760,
hideThumbs:200,

thumbWidth:100,
thumbHeight:50,
thumbAmount:5,

navigationType:"none",
navigationArrows:"solo",
navigationStyle:"preview0",

hideTimerBar:"on",

touchenabled:"on",
onHoverStop:"on",

swipe_velocity: 0.7,
swipe_min_touches: 1,
swipe_max_touches: 1,
drag_block_vertical: false,

parallax:"mouse",
parallaxBgFreeze:"on",
parallaxLevels:[0],
parallaxDisableOnMobile:"on",

keyboardNavigation:"off",

navigationHAlign:"center",
navigationVAlign:"bottom",
navigationHOffset:0,
navigationVOffset:20,
soloArrowLeftHalign:"left",
soloArrowLeftValign:"center",
soloArrowLeftHOffset:20,
soloArrowLeftVOffset:0,
soloArrowRightHalign:"right",
soloArrowRightValign:"center",
soloArrowRightHOffset:20,
soloArrowRightVOffset:0,

shadow:0,
fullWidth:"off",
fullScreen:"on",
spinner:"spinner4",

stopLoop:"off",
stopAfterLoops:-1,
stopAtSlide:-1,
shuffle:"off",

autoHeight:"off",
forceFullWidth:"off",

hideThumbsOnMobile:"off",
hideNavDelayOnMobile:1500,
hideBulletsOnMobile:"off",
hideArrowsOnMobile:"off",
hideThumbsUnderResolution:0,

hideSliderAtLimit:0,
hideCaptionAtLimit:0,
hideAllCaptionAtLilmit:0,
startWithSlide:0,

});
}
}); //ready

</script>