<div class="header-transporent-bg-black">
	[menu]
	
	<div id="index-link" class="sm-video-bg" style="background-image: url([base_url]theme/theme/images/static-media/the-crossswalk.jpg);" >
		<div class="container sm-content-cont js-height-fullscr">
			
			<!-- VIDEO BG -->
			<!-- If you want to change video - replace video files in folder "video" -->
			<div class="sm-video-wrapper">
				<div class="sm-video bg-img-alfa"></div>
			</div>
			
		</div>
		<!-- SCROLL ICON -->
		<div class="local-scroll-cont">
			<a href="#about" class="scroll-down smooth-scroll">
				<div class="icon icon-arrows-down"></div>
			</a>
		</div>
	</div>
	
	<!-- FEATURES 5 & TESTIMONIALS 1 -->
	<div class="page-section p-110-cont">
		<div class="container">
			<div class="row">
				<!-- TESTIMONIALS -->
				<div class="col-md-5 pb-40">
					<blockquote class="quote mb-0 pr-50-min-1169">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, maiores esse temporibus actium quas soluta quis sed rerum.</p>
					<footer>John Doe, Google Inc.</footer>
				</blockquote>
			</div>
			
			<!-- FEATURES -->
			<div class="col-md-7">
				
				<div class="row">
					
					<div class="col-md-6 col-sm-6 pb-10">
						<div class="fes5-box wow fadeIn" >
							<h3>FULLY RESPONSIVE</h3>
							<p>Sed ut perspiciatis unde omnis iste nat eror acus  antium que</p>
						</div>
					</div>
					
					<div class="col-md-6 col-sm-6 pb-10">
						<div class="fes5-box wow fadeIn" data-wow-delay="200ms">
							<h3>RETINA READY</h3>
							<p>Sed ut perspiciatis unde omnis iste nat eror acus  antium que</p>
						</div>
					</div>
					
				</div>
				
				<div class="row">
					
					<div class="col-md-6 col-sm-6 pb-10">
						<div class="fes5-box wow fadeIn" data-wow-delay="400ms">
							<h3>UNIQUE DESIGN</h3>
							<p>Sed ut perspiciatis unde omnis iste nat eror acus  antium que</p>
						</div>
					</div>
					
					<div class="col-md-6 col-sm-6 pb-10">
						<div class="fes5-box wow fadeIn"  data-wow-delay="600ms">
							<h3>EASY TO CUSTOMIZE</h3>
							<p>Sed ut perspiciatis unde omnis iste nat eror acus  antium que</p>
						</div>
					</div>
					
				</div>
				
			</div>
			
		</div>
	</div>
</div>
<!-- FEATURES 7 -->
<div class="page-section grey-light-bg clearfix">
	<div class="fes7-img-cont col-md-5">
		<div class="fes7-img" style="background-image: url(images/fes7-2.jpg)"></div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-6 fes7-text-cont p-80-cont">
				<h1><span class="font-light">Nulla varius faucibus vestibulum. Sed imperdiet, tellus at iaculis</span></h1>
				<p class="mb-60">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.
				</p>
				
				<div class="row">
					
					<div class="col-md-6 col-sm-6">
						<div class="fes7-box wow fadeIn" >
							<div class="fes7-box-icon">
								<div class="icon icon-ecommerce-graph-increase"></div>
							</div>
							<h3>SEO Friendly</h3>
							<p>Sed ut perspiciatis unde omnis iste nat eror acus  antium que</p>
						</div>
					</div>
					
					<div class="col-md-6 col-sm-6">
						<div class="fes7-box wow fadeIn" data-wow-delay="200ms">
							<div class="fes7-box-icon">
								<div class="icon icon-software-font-smallcaps"></div>
							</div>
							<h3>Google Fonts</h3>
							<p>Sed ut perspiciatis unde omnis iste nat eror acus  antium que</p>
						</div>
					</div>
					
				</div>
				
				<div class="row">
					
					<div class="col-md-6 col-sm-6">
						<div class="fes7-box wow fadeIn" data-wow-delay="400ms">
							<div class="fes7-box-icon">
								<div class="icon icon-basic-mixer2"></div>
							</div>
							<h3>Tons of Shortcodes</h3>
							<p>Sed ut perspiciatis unde omnis iste nat eror acus  antium que</p>
						</div>
					</div>
					
					<div class="col-md-6 col-sm-6">
						<div class="fes7-box wow fadeIn"  data-wow-delay="600ms">
							<div class="fes7-box-icon">
								<div class="icon icon-basic-bolt"></div>
							</div>
							<h3>1500+ Icons</h3>
							<p>Sed ut perspiciatis unde omnis iste nat eror acus  antium que</p>
						</div>
					</div>
					
				</div>
				
			</div>
			</div><!--end of row-->
		</div>
	</div>
	
	<!-- DIVIDER -->
	<hr class="mt-0 mb-0">
	<!-- FEATURES 4 -->
	<div class="page-section fes4-cont">
		<div class="container">
			<div class="row">
				
				<div class="col-xs-12 col-sm-4 col-md-4">
					<div class="fes4-box wow fadeIn">
						<h2 class="section-title">OUR <span class="bold">SERVICES</span></h2>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4">
					<div class="fes4-box wow fadeIn" data-wow-delay="200ms">
						<div class="fes4-title-cont" >
							<div class="fes4-box-icon">
								<div class="icon icon-basic-settings"></div>
							</div>
							<h3><span class="bold">DEVELOPMENT</span></h3>
							<p>LOREM IPSUM DOLOR</p>
						</div>
						<div>
							Maecenas luctus nisi in sem fermentum blat. In nec  elit solliudin, elementum, dictum pur quam volutpat suscipit antena.
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4">
					<div class="fes4-box wow fadeIn" data-wow-delay="400ms">
						<div class="fes4-title-cont" >
							<div class="fes4-box-icon">
								<div class="icon icon-basic-share"></div>
							</div>
							<h3><span class="bold">PRODUCTION</span></h3>
							<p>LOREM IPSUM DOLOR</p>
						</div>
						<div>
							Seductio maesto nisi in sem fermentum blat. In nec elit solliudin, elementum, dictum pur quam volutpat suscipit antena.
						</div>
					</div>
				</div>
				
			</div>
			<div class="row">
				
				<div class="col-xs-12 col-sm-4 col-md-4">
					<div class="fes4-box wow fadeIn" data-wow-delay="600ms">
						<div class="fes4-title-cont" >
							<div class="fes4-box-icon">
								<div class="icon icon-basic-target"></div>
							</div>
							<h3><span class="bold">BRANDING</span></h3>
							<p>LOREM IPSUM DOLOR</p>
						</div>
						<div>
							Donec vel luctus nisi in sem fermentum blat. In nec elit solliudin, elementum, dictum pur quam volutpat suscipit antena.
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4">
					<div class="fes4-box wow fadeIn" data-wow-delay="800ms" >
						<div class="fes4-title-cont" >
							<div class="fes4-box-icon">
								<div class="icon icon-basic-globe"></div>
							</div>
							<h3><span class="bold">WEB DESIGN</span></h3>
							<p>LOREM IPSUM DOLOR</p>
						</div>
						<div>
							Lorem luctus antena at nisi in sem blandit. In nec elit soltudin, elementum odio et, dictum quam a volutpat elementum.
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4">
					<div class="fes4-box wow fadeIn" data-wow-delay="1000ms">
						<div class="fes4-title-cont" >
							<div class="fes4-box-icon">
								<div class="icon icon-basic-picture"></div>
							</div>
							<h3><span class="bold">PHOTOGRAPHY</span></h3>
							<p>LOREM IPSUM DOLOR</p>
						</div>
						<div>
							Fermentum nisi in sem fertum blat. In elit soldin, elemtum, arenam pur quam volutpat suscipit dictum pur quam.
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<!-- PORTFOLIO SECTION 1 -->
	<div class="page-section">
		<div class="relative">
			<!-- ITEMS GRID -->
			<ul class="port-grid clearfix masonry" id="items-grid">
				
				<!-- Item 1 -->
				<li class="port-item mix">
					<a href="portfolio-single1.html">
						<div class="port-img-overlay"><img class="port-main-img" src="[base_url]theme/theme/images/portfolio/projects-1.jpg" alt="img" ></div>
					</a>
					<div class="port-overlay-cont">
						<div class="port-title-cont">
							<h3><a href="portfolio-single1.html">MINIMALISM BOOKS</a></h3>
							<span><a href="#">ui elements</a> / <a href="#">media</a></span>
						</div>
						<div class="port-btn-cont">
							<a href="[base_url]theme/theme/images/portfolio/projects-2-big.jpg" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
							<a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
						</div>
					</div>
				</li>
				<!-- Item 2 -->
				<li class="port-item mix">
					<a href="portfolio-single1.html">
						<div class="port-img-overlay"><img class="port-main-img" src="[base_url]theme/theme/images/portfolio/projects-3-big.jpg" alt="img" ></div>
					</a>
					<div class="port-overlay-cont">
						
						<div class="port-title-cont">
							<h3><a href="portfolio-single1.html">CALENDAR</a></h3>
							<span><a href="#">photography</a> / <a href="#">media</a></span>
						</div>
						<div class="port-btn-cont">
							<a href="[base_url]theme/theme/images/portfolio/projects-2-big.jpg" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
							<a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
						</div>
						
					</div>
				</li>
				
				<!-- Item 3 -->
				<li class="port-item mix">
					<a href="portfolio-single1.html">
						<div class="port-img-overlay"><img class="port-main-img" src="[base_url]theme/theme/images/portfolio/projects-5-very-big.jpg" alt="img" ></div>
					</a>
					<div class="port-overlay-cont">
						
						<div class="port-title-cont">
							<h3><a href="portfolio-single1.html">EYES OF THE CAR</a></h3>
							<span><a href="#">branding</a> / <a href="#">marketing</a></span>
						</div>
						<div class="port-btn-cont">
							<a href="[base_url]theme/theme/images/portfolio/projects-2-big.jpg" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
							<a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
						</div>
						
					</div>
				</li>
				
				<!-- Item 4 -->
				<li class="port-item mix">
					<a href="portfolio-single1.html">
						<div class="port-img-overlay"><img class="port-main-img" src="[base_url]theme/theme/images/portfolio/projects-6-big.jpg" alt="img" ></div>
					</a>
					<div class="port-overlay-cont">
						
						<div class="port-title-cont">
							<h3><a href="portfolio-single1.html">NOW IS NOW</a></h3>
							<span><a href="#">design</a> / <a href="#">photography</a></span>
						</div>
						<div class="port-btn-cont">
							<a href="[base_url]theme/theme/images/portfolio/projects-2-big.jpg" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
							<a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
						</div>
						
					</div>
				</li>
				
				<!-- Item 5 -->
				<li class="port-item mix">
					<a href="portfolio-single1.html">
						<div class="port-img-overlay"><img class="port-main-img" src="[base_url]theme/theme/images/portfolio/projects-2-big.jpg" alt="img" ></div>
					</a>
					<div class="port-overlay-cont">
						
						<div class="port-title-cont">
							<h3><a href="portfolio-single1.html">EYES OF THE CAR</a></h3>
							<span><a href="#">ui elements</a> / <a href="#">media</a></span>
						</div>
						<div class="port-btn-cont">
							<a href="[base_url]theme/theme/images/portfolio/projects-2-big.jpg" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
							<a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
						</div>
						
					</div>
				</li>
				
				<!-- Item 6 -->
				<li class="port-item mix">
					<a href="portfolio-single1.html">
						<div class="port-img-overlay"><img class="port-main-img" src="[base_url]theme/theme/images/portfolio/projects-4.jpg" alt="img" ></div>
					</a>
					<div class="port-overlay-cont">
						
						<div class="port-title-cont">
							<h3><a href="portfolio-single1.html">LOVE</a></h3>
							<span><a href="#">branding</a> / <a href="#">media</a></span>
						</div>
						<div class="port-btn-cont">
							<a href="[base_url]theme/theme/images/portfolio/projects-2-big.jpg" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
							<a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
						</div>
						
					</div>
				</li>
				
				<!-- Item 7 -->
				<li class="port-item mix">
					<a href="portfolio-single1.html">
						<div class="port-img-overlay"><img class="port-main-img" src="[base_url]theme/theme/images/portfolio/projects-7.jpg" alt="img" ></div>
					</a>
					<div class="port-overlay-cont">
						
						<div class="port-title-cont">
							<h3><a href="portfolio-single1.html">YELLOW BOOK</a></h3>
							<span><a href="#">design</a> / <a href="#">media</a></span>
						</div>
						<div class="port-btn-cont">
							<a href="[base_url]theme/theme/images/portfolio/projects-2-big.jpg" class="lightbox mr-20" ><div aria-hidden="true" class="icon_search"></div></a>
							<a href="portfolio-single1.html"><div aria-hidden="true" class="icon_link"></div></a>
						</div>
						
					</div>
				</li>
				
			</ul>
			
		</div>
	</div>
	
	<!-- VIEW MORE PROJECTS  -->
	<div class="port-view-more-cont">
		<a class="port-view-more" href="portfolio-4-col-masonry.html">VIEW MORE PROJECTS</a>
	</div>
	<!-- END PORTFOLIO SECTION 1 -->
	<!-- DIVIDER -->
	<hr class="mt-0 mb-0">
	
	<!-- CLIENTS 1 & TESTIMONIALS 1 -->
	<div class="page-section p-110-cont">
		<div class="container">
			<div class="row">
				
				<div class="col-md-12">
					<div class="mb-50">
						<h2 class="section-title">OUR <span class="bold">CLIENTS</span></h2>
					</div>
				</div>
				
			</div>
			<div class="row">
				<!-- CLIENTS 1 -->
				<div class="col-md-7">
					<div class="row client-row border-bot">
						<div class="col-xs-6 col-sm-3 text-center">
							<img alt="client" src="[base_url]theme/theme/images/clients/1.png">
						</div>
						
						<div class="col-xs-6 col-sm-3 text-center">
							<img alt="client" src="[base_url]theme/theme/images/clients/2.png">
						</div>
						
						<div class="col-xs-6 col-sm-3 text-center">
							<img alt="client" src="[base_url]theme/theme/images/clients/3.png">
						</div>
						
						<div class="col-xs-6 col-sm-3 text-center">
							<img alt="client" src="[base_url]theme/theme/images/clients/4.png">
						</div>
					</div>
					
					<div class="row client-row">
						<div class="col-xs-6 col-sm-3 text-center">
							<img alt="client" src="[base_url]theme/theme/images/clients/5.png">
						</div>
						
						<div class="col-xs-6 col-sm-3 text-center">
							<img alt="client" src="[base_url]theme/theme/images/clients/6.png">
						</div>
						
						<div class="col-xs-6 col-sm-3 text-center">
							<img alt="client" src="[base_url]theme/theme/images/clients/7.png">
						</div>
						
						<div class="col-xs-6 col-sm-3 text-center">
							<img alt="client" src="[base_url]theme/theme/images/clients/8.png">
						</div>
					</div>
					
				</div>
				<!-- TESTIMONIALS 1 -->
				<div class="col-md-5">
					<blockquote class="quote mb-0 m-p-0">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, maiores esse temporibus accusantium quas soluta quis sed rerum.</p>
					<footer>John Doe, Google Inc.</footer>
				</blockquote>
			</div>
			
		</div>
	</div>
</div>
<!-- WORK PROCESS 1 -->
<div class="page-section work-proc-1-bg" >
	<div class="container fes4-cont">
		<div class="row">
			
			<div class="col-md-4 ">
				<div class="mb-50">
					<h2 class="section-title">WORK <span class="bold">PROCESS</span></h2>
				</div>
				<p class="mb-50 " >Sed ut perspiciatis unde omnis iste nat eror acus  antium que</p>
			</div>
			<div class="col-md-7 col-lg-offset-1">
				<div class="row">
					
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="fes4-box">
							<div class="fes4-title-cont" >
								<div class="fes4-box-icon">
									<div class="icon icon-basic-lightbulb"></div>
								</div>
								<h3><span class="bold">PLANING</span></h3>
								<p>LOREM IPSUM DOLOR</p>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="fes4-box" >
							<div class="fes4-title-cont" >
								<div class="fes4-box-icon">
									<div class="icon icon-basic-gear"></div>
								</div>
								<h3><span class="bold">DEVELOPMENT</span></h3>
								<p>LOREM IPSUM DOLOR</p>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="fes4-box" >
							<div class="fes4-title-cont" >
								<div class="fes4-box-icon">
									<div class="icon icon-basic-laptop"></div>
								</div>
								<h3><span class="bold">DESIGNING</span></h3>
								<p>LOREM IPSUM DOLOR</p>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="fes4-box" >
							<div class="fes4-title-cont" >
								<div class="fes4-box-icon">
									<div class="icon icon-basic-paperplane"></div>
								</div>
								<h3><span class="bold">LAUNCH</span></h3>
								<p>LOREM IPSUM DOLOR</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- ABOUT US 1 -->
<div class="page-section pt-110-cont">
	<div class="container">
		<div class="row">
			
			<div class="col-md-12">
				<div class="mb-50">
					<h2 class="section-title">OUR <span class="bold">TEAM</span></h2>
				</div>
			</div>
			
		</div>
		
		<div class="row">
			
			<div class="member col-md-4 col-sm-4 wow fadeInUp">
				<div class="member-image">
					<img src="[base_url]theme/theme/images/team/team-1.jpg" alt="img">
				</div>
				<h3>JESSICA DOE</h3>
				<span>VP ENGINEERING</span>
				<ul class="team-social">
					<li><a href="#"><span aria-hidden="true" class="social_facebook"></span></a></li>
					<li><a href="#"><span aria-hidden="true" class="social_twitter"></span></a></li>
					<li><a href="#"><span aria-hidden="true" class="social_dribbble"></span></a></li>
				</ul>
			</div>
			
			<div class="member col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="200ms">
				<div class="member-image">
					<img src="[base_url]theme/theme/images/team/team-2.jpg" alt="img">
				</div>
				<h3>JOHN DOE</h3>
				<span>FOUNDER AND CEO</span>
				<ul class="team-social">
					<li><a href="#"><span aria-hidden="true" class="social_facebook"></span></a></li>
					<li><a href="#"><span aria-hidden="true" class="social_twitter"></span></a></li>
					<li><a href="#"><span aria-hidden="true" class="social_dribbble"></span></a></li>
				</ul>
			</div>
			
			<div class="member col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="400ms">
				<div class="member-image">
					<img src="[base_url]theme/theme/images/team/team-3.jpg" alt="img">
				</div>
				<h3>EDWARD DOE</h3>
				<span>CREATIVE DIRECTOR</span>
				<ul class="team-social">
					<li><a href="#"><span aria-hidden="true" class="social_facebook"></span></a></li>
					<li><a href="#"><span aria-hidden="true" class="social_twitter"></span></a></li>
					<li><a href="#"><span aria-hidden="true" class="social_dribbble"></span></a></li>
				</ul>
			</div>
			
		</div>
	</div>
</div>
<!-- VIEW MORE PROJECTS  -->
<div class="port-view-more-cont">
	<a class="port-view-more" href="about-us.html">VIEW ALL TEAM</a>
</div>
<!-- COUNTERS 1 -->
<div id="counter-1" class="page-section p-80-cont">
	<div class="container">
		
		<div  class="row text-center">
			
			<!-- Item -->
			<div class="col-xs-6 col-sm-3">
				<div class="count-number">
					75
				</div>
				<div class="count-descr">
					<span class="count-title">AWARDS WINNING</span>
				</div>
			</div>
			
			<!-- Item -->
			<div class="col-xs-6 col-sm-3">
				<div class="count-number">
					450
				</div>
				<div class="count-descr">
					<span class="count-title">HAPPY CLIENTS</span>
				</div>
			</div>
			
			<!-- Item -->
			<div class="col-xs-6 col-sm-3">
				<div class="count-number">
					151
				</div>
				<div class="count-descr">
					<span class="count-title">PROJECTS DONE</span>
				</div>
			</div>
			
			<!-- Item -->
			<div class="col-xs-6 col-sm-3">
				<div class="count-number">
					768
				</div>
				<div class="count-descr">
					<span class="count-title">HOURS OF CODE</span>
				</div>
			</div>
			
		</div>
	</div>
</div>
<!-- DIVIDER -->
<hr class="mt-0 mb-0">
<!-- ADS 1 -->
<div class="page-section">
	<div class="container">
		<div class="row">
			
			<div class="col-md-6 left-50">
				<div class="fes2-main-text-cont">
					<div class="title-fs-45">
						OPTIMIZED FOR<br>
						<span class="bold">MOBILE</span>
					</div>
					<div class="line-3-100"></div>
					<div class="fes2-text-cont">Sed ut perspiciatis unde omnis iste nat eror acus  antium que. Asperiores, ea velit enim labore doloribus.</div>
					<div class="mt-30">
						<a class="button medium thin hover-dark" href="http://themeforest.net/user/abcgomel/portfolio?ref=abcgomel">BUY NOW</a>
					</div>
				</div>
			</div>
			
			<div class="col-md-6 right-50 wow fadeInLeft">
				
				<div class="ads-img-cont" >
					<img src="[base_url]theme/theme/images/ads-phone.jpg" alt="img">
				</div>
				
			</div>
			
		</div>
	</div>
</div>
<!-- DIVIDER -->
<hr class="mt-0 mb-0">
<!-- ADS 2 -->
<div class="page-section">
	<div class="container">
		<div class="row">
			
			<div class="col-md-6  ">
				<div class="fes2-main-text-cont">
					<div class="title-fs-45">
						POWERFUL<br>
						<span class="bold">OPTIONS</span>
					</div>
					<div class="line-3-100"></div>
					<div class="fes2-text-cont">Sed ut perspiciatis unde omnis iste nat eror acus  antium que. Asperiores, ea velit enim labore doloribus.</div>
					<div class="mt-30">
						<a class="button medium thin hover-dark" href="http://themeforest.net/user/abcgomel/portfolio?ref=abcgomel">BUY NOW</a>
					</div>
				</div>
			</div>
			
			<div class="col-md-6 wow fadeInRight">
				
				<div class="ads-img-cont" >
					<img src="[base_url]theme/theme/images/ads-noute.jpg" alt="img">
				</div>
				
			</div>
			
		</div>
	</div>
</div>
<!-- VIDEO ADS 1 -->
<div class="page-section video-ads-bg" >
	<div class="container">
		<div class="video-ads-text-cont clearfix">
			<span class="video-ads-text">BE CREATIVE</span>
			<span class="video-ads-a">
				<a class="popup-youtube" href="https://www.youtube.com/watch?v=0gv7OC9L2s8">
					<span class="icon icon-music-play-button"></span>
				</a>
			</span>
			<span class="video-ads-text">WITH HASWELL</span>
		</div>
	</div>
</div>
<!-- BLOG 1 -->
<div class="page-section pt-110-b-30-cont">
	<div class="container">
		
		<div class="mb-50">
			<h2 class="section-title pr-0">LATEST <span class="bold">NEWS</span><a href="blog-right-sidebar.html" class="section-more right">OUR BLOG</a>
			</h2>
		</div>
		
		<div class="row">
			
			<!-- Post Item 1 -->
			<div class="col-sm-6 col-md-4 col-lg-4 wow fadeIn pb-70" >
				
				<div class="post-prev-img">
					<a href="blog-single-sidebar-right.html"><img src="[base_url]theme/theme/images/blog/post-prev-1.jpg" alt="img"></a>
				</div>
				
				<div class="post-prev-title">
					<h3><a href="blog-single-sidebar-right.html">TIME FOR MINIMALISM</a></h3>
				</div>
				
				<div class="post-prev-info">
					JULE 10<span class="slash-divider">/</span><a href="http://themeforest.net/user/abcgomel/portfolio?ref=abcgomel">JOHN DOE</a>
				</div>
				
				<div class="post-prev-text">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, nostrum, cumque culpa provident aliquam commodi assumenda laudantium magnam illo nostrum.
				</div>
				
				<div class="post-prev-more-cont clearfix">
					<div class="post-prev-more left">
						<a href="blog-single-sidebar-right.html" class="blog-more">READ MORE</a>
					</div>
					<div class="right" >
						<a href="blog-single-sidebar-right.html#comments" class="post-prev-count"><span aria-hidden="true" class="icon_comment_alt"></span><span class="icon-count">21</span></a>
						<a href="http://themeforest.net/user/abcgomel/portfolio?ref=abcgomel" class="post-prev-count"><span aria-hidden="true" class="icon_heart_alt"></span><span class="icon-count">53</span></a>
						<a href="#" class="post-prev-count dropdown-toggle" data-toggle="dropdown" aria-expanded="false" >
							<span aria-hidden="true" class="social_share"></span>
						</a>
						<ul class="social-menu dropdown-menu dropdown-menu-right" role="menu">
							<li><a href="#"><span aria-hidden="true" class="social_facebook"></span></a>
						</li>
						<li><a href="#"><span aria-hidden="true" class="social_twitter"></span></a></li>
						<li><a href="#"><span aria-hidden="true" class="social_dribbble"></span></a></li>
					</ul>
				</div>
			</div>
			
		</div>
		
		<!-- Post Item 2 -->
		<div class="col-sm-6 col-md-4 col-lg-4 wow fadeIn pb-70" data-wow-delay="200ms" >
			
			<div class="post-prev-img">
				<a href="blog-single-sidebar-right.html"><img src="[base_url]theme/theme/images/blog/post-prev-2.jpg" alt="img"></a>
			</div>
			
			<div class="post-prev-title">
				<h3><a href="blog-single-sidebar-right.html">NEW TRENDS IN WEB DESIGN</a></h3>
			</div>
			
			<div class="post-prev-info">
				MAY 11<span class="slash-divider">/</span><a href="http://themeforest.net/user/abcgomel/portfolio?ref=abcgomel">JOHN DOE</a>
			</div>
			
			<div class="post-prev-text">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, nostrum, cumque culpa provident aliquam commodi assumenda laudantium magnam illo nostrum.
			</div>
			
			<div class="post-prev-more-cont clearfix">
				<div class="post-prev-more left">
					<a href="blog-single-sidebar-right.html" class="blog-more">READ MORE</a>
				</div>
				<div class="right" >
					<a href="blog-single-sidebar-right.html#comments" class="post-prev-count"><span aria-hidden="true" class="icon_comment_alt"></span><span class="icon-count">21</span></a>
					<a href="http://themeforest.net/user/abcgomel/portfolio?ref=abcgomel" class="post-prev-count"><span aria-hidden="true" class="icon_heart_alt"></span><span class="icon-count">53</span></a>
					<a href="#" class="post-prev-count dropdown-toggle" data-toggle="dropdown" aria-expanded="false" >
						<span aria-hidden="true" class="social_share"></span>
					</a>
					<ul class="social-menu dropdown-menu dropdown-menu-right" role="menu">
						<li><a href="#"><span aria-hidden="true" class="social_facebook"></span></a>
					</li>
					<li><a href="#"><span aria-hidden="true" class="social_twitter"></span></a></li>
					<li><a href="#"><span aria-hidden="true" class="social_dribbble"></span></a></li>
				</ul>
			</div>
		</div>
		
	</div>
	
	<!-- Post Item 3 -->
	<div class="col-sm-6 col-md-4 col-lg-4 wow fadeIn pb-70" data-wow-delay="400ms" >
		
		<div class="post-prev-img">
			<a href="blog-single-sidebar-right.html"><img src="[base_url]theme/theme/images/blog/post-prev-3.jpg" alt="img"></a>
		</div>
		
		<div class="post-prev-title">
			<h3><a href="blog-single-sidebar-right.html">THE SOUND OF LIFE</a></h3>
		</div>
		
		<div class="post-prev-info">
			DECEMBER 21<span class="slash-divider">/</span><a href="http://themeforest.net/user/abcgomel/portfolio?ref=abcgomel">JOHN DOE</a>
		</div>
		
		<div class="post-prev-text">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, nostrum, cumque culpa provident aliquam commodi assumenda laudantium magnam illo nostrum.
		</div>
		
		<div class="post-prev-more-cont clearfix">
			<div class="post-prev-more left">
				<a href="blog-single-sidebar-right.html" class="blog-more">READ MORE</a>
			</div>
			<div class="right" >
				<a href="blog-single-sidebar-right.html#comments" class="post-prev-count"><span aria-hidden="true" class="icon_comment_alt"></span><span class="icon-count">21</span></a>
				<a href="http://themeforest.net/user/abcgomel/portfolio?ref=abcgomel" class="post-prev-count"><span aria-hidden="true" class="icon_heart_alt"></span><span class="icon-count">53</span></a>
				<a href="#" class="post-prev-count dropdown-toggle" data-toggle="dropdown" aria-expanded="false" >
					<span aria-hidden="true" class="social_share"></span>
				</a>
				<ul class="social-menu dropdown-menu dropdown-menu-right" role="menu">
					<li><a href="#"><span aria-hidden="true" class="social_facebook"></span></a>
				</li>
				<li><a href="#"><span aria-hidden="true" class="social_twitter"></span></a></li>
				<li><a href="#"><span aria-hidden="true" class="social_dribbble"></span></a></li>
			</ul>
		</div>
	</div>
	
</div>
</div>
</div>
</div>
[footer]