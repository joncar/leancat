<!-- NEWS LETTER -->
<div class="page-section nl-cont">
	<div class="container">
		<div class="relative" >
			<div id="mc_embed_signup" class="nl-form-container clearfix">
				<form action="http://abcgomel.us9.list-manage.com/subscribe/post-json?u=ba37086d08bdc9f56f3592af0&amp;id=e38247f7cc&amp;c=?" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="newsletterform validate" target="_blank" novalidate>   <!-- EDIT THIS ACTION URL (add "post-json?u" instead of "post?u" and appended "&amp;c=?" to the end of this URL) -->
				<input type="email" value="" name="EMAIL" class="email nl-email-input" id="mce-EMAIL" placeholder="Enter your email" required>
				<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
				<div style="position: absolute; left: -5000px;"><input type="text" name="b_ba37086d08bdc9f56f3592af0_e38247f7cc" tabindex="-1" value=""></div>
				
				<input type="submit" value="SUBSCRIBE" name="subscribe" id="mc-embedded-subscribe" class="button medium gray">
			</form>
			<div id="notification_container"  ></div>
		</div>
	</div>
</div>
</div>
<!-- FOOTER 3 BLACK  -->
<footer id="footer4" class="page-section pt-80 pb-50 footer2-black">
<div class="container-m-60">
	<div class="row">
		
		<div class="col-md-3 col-sm-3 widget">
			<div class="logo-footer-cont">
				<a href="index.html">
					<img class="logo-footer" src="[base_url]theme/theme/images/logo-footer-white.png" alt="logo">
				</a>
			</div>
			<div class="footer-2-text-cont">
				<address>
					555 California str, Suite 100<br>
					San&nbsp;Francisco, CA 94107
				</address>
			</div>
			<div class="footer-2-text-cont">
				1-800-312-2121<br>
				1-800-310-1010
			</div>
			<div class="footer-2-text-cont">
				<a class="a-text" href="mailto:info@haswell.com">info@haswell.com</a>
			</div>
		</div>
		
		<div class="col-md-3 col-sm-3 widget">
			<h4>NAVIGATE</h4>
			<ul class="links-list bold a-text-cont">
				<li><a href="index.html">HOME</a></li>
				<li><a href="grid-system.html">GRID SYSTEM</a></li>
				<li><a href="services.html">SERVICES</a></li>
				<li><a href="index-portfolio.html">PORTFOLIO</a></li>
				<li><a href="index-blog.html">BLOG</a></li>
				<li><a href="index-shop.html">SHOP</a></li>
				<li><a href="intro.html">PAGES</a></li>
			</ul>
		</div>
		
		<div class="col-md-3 col-sm-3 widget">
			<h4>ABOUT US</h4>
			<ul class="links-list a-text-cont" >
				<li><a href="about-us.html">COMPANY</a></li>
				<li><a href="services.html">WHAT WE DO</a></li>
				<li><a href="https://help.market.envato.com/hc/en-us">HELP CENTER</a></li>
				<li><a href="http://themeforest.net/legal/market">TERMS OF SERVICE</a></li>
				<li><a href="contact.html">CONTACT</a></li>
			</ul>
		</div>
		
		<div class="col-md-3 col-sm-3 widget">
			<h4>RECENT POSTS</h4>
			<div id="post-list-footer">
				<div class="post-prev-title">
					<h3><a class="a-text"  href="blog-single-sidebar-right.html">New trends in web design</a></h3>
				</div>
				<div class="post-prev-info">
					Jule 10
				</div>
				
				<div class="post-prev-title">
					<h3><a class="a-text"  href="blog-single-sidebar-right.html">The sound of life</a></h3>
				</div>
				<div class="post-prev-info">
					October 10
				</div>
				
				<div class="post-prev-title">
					<h3><a class="a-text"  href="blog-single-sidebar-right.html">Time for minimalism</a></h3>
				</div>
				<div class="post-prev-info">
					September 21
				</div>
			</div>
		</div>
	</div>
	
	<div class="footer-2-copy-cont clearfix">
		<!-- Social Links -->
		<div class="footer-2-soc-a right">
			<a href="http://themeforest.net/user/abcgomel/portfolio?ref=abcgomel" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a>
			<a href="http://themeforest.net/user/abcgomel/portfolio?ref=abcgomel" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a>
			<a href="http://themeforest.net/user/abcgomel/portfolio?ref=abcgomel" title="Behance" target="_blank"><i class="fa fa-behance"></i></a>
			<a href="http://themeforest.net/user/abcgomel/portfolio?ref=abcgomel" title="LinkedIn+" target="_blank"><i class="fa fa-linkedin"></i></a>
			<a href="http://themeforest.net/user/abcgomel/portfolio?ref=abcgomel" title="Dribbble" target="_blank"><i class="fa fa-dribbble"></i></a>
		</div>
		
		<!-- Copyright -->
		<div class="left">
			<a class="footer-2-copy" href="http://themeforest.net/user/abcgomel/portfolio?ref=abcgomel" target="_blank">&copy; HASWELL 2018</a>
		</div>
		
	</div>
	
</div>
</footer>