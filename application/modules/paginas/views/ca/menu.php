<!-- HEADER 1 BLACK MOBILE-NO-TRANSPARENT -->
<header id="nav" class="header header-1 black-header affix-on-mobile">
	<!-- TOP BAR -->
	<div class="top-bar">
		<div class="container-m-30 clearfix">
			
			<!-- LEFT SECTION -->
			<ul class="top-bar-section left display-no-xxs">
				<li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
				<li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
				<li><a href="#" title="Linked"><i class="fa fa-linkedin"></i></a></li>
				<li><a href="#" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
			</ul>
			
			<!-- RIGHT SECTION -->
			<ul class="top-bar-section right">
				<li><a href="tel:138376628"><i class="fa fa-phone mr-5"></i> 1-800-312-212</a></li>
				<li><i class="fa  fa-clock-o mr-5"></i> Mon - Fri: 08.00 - 17.00</li>
				<li class="display-no-xxs"><a href="mailto:info@haswell.com?subject=Order" title="E-mail"><i class="fa fa-envelope mr-7"></i>info@haswell.com</a></li>
			</ul>
			
		</div>
	</div>
	
	<div class="header-wrapper">
		<div class="container-m-30 clearfix">
			<div class="logo-row">
				
				<!-- LOGO -->
				<div class="logo-container-2">
					<div class="logo-2">
						<a href="<?= site_url() ?>" class="clearfix">
							<img src="[base_url]theme/theme/images/logo-white.png" class="logo-img" alt="Logo">
						</a>
					</div>
				</div>
				<!-- BUTTON -->
				<div class="menu-btn-respons-container">
					<button type="button" class="navbar-toggle btn-navbar collapsed" data-toggle="collapse" data-target="#main-menu .navbar-collapse">
					<span aria-hidden="true" class="icon_menu hamb-mob-icon"></span>
					</button>
				</div>
			</div>
		</div>
		<!-- MAIN MENU CONTAINER -->
		<div class="main-menu-container">
			
			<div class="container-m-30 clearfix">
				
				<!-- MAIN MENU -->
				<div id="main-menu">
					<div class="navbar navbar-default" role="navigation">
						<!-- MAIN MENU LIST -->
						<nav class="collapse collapsing navbar-collapse right-1024">
							<ul class="nav navbar-nav">
								<li class="current"><a class="current" href="<?= site_url() ?>"><div class="main-menu-title">INICI</div></a></li>
								<li class="parent">
									<a href="#">NOSALTRES</a>
									<ul class="sub">
										<li><a href="#">Porque Leancat</a></li>
										<li><a href="#">Porque Equipo</a></li>
									</ul>
								</li>
								<li class="parent">
									<a href="#">SERVEIS</a>
									<ul class="sub">
										<li><a href="#">Implantacion leancat</a></li>
										<li><a href="#">Servicios de ingenieria</a></li>
									</ul>
								</li>
								<li>
									<a href="<?= base_url('blog') ?>">BLOGS</a>
								</li>
								<li>
									<a href="<?= base_url('blog') ?>">JOCS</a>
								</li>
								<!-- MENU ITEM -->
							<li id="menu-contact-info-big" class="parent megamenu">
								<a href="#"><div class="main-menu-title">CONTACTA</div></a>
								<ul class="sub">
									<li class="clearfix" >
										<div class="menu-sub-container">
											<div class="box col-md-3 menu-demo-info closed">
												<h5 class="title">CONTACT PAGES</h5>
												<ul>
													<li><a href="contact.html">Contact Version 1</a></li>
													<li><a href="contact2.html">Contact Version 2</a></li>
													<li><a href="contact-recaptcha.html">Contact reCAPTCHA<span class="label-new">NEW</span></a></li>
													<li><a href="contact2-recaptcha.html">Contact 2 reCAPTCHA<span class="label-new">NEW</span></a></li>
												</ul>
											</div>
											
											<div class="col-md-3 menu-contact-info">
												<ul class="contact-list">
													<li class="contact-loc clearfix">
														<div class="loc-icon-container">
															<span aria-hidden="true" class="icon_pin_alt main-menu-contact-icon"></span>
														</div>
														<div class="menu-contact-text-container">555 California str, Suite 100</div>
													</li>
													<li class="contact-phone clearfix">
														<div class="loc-icon-container">
															<span aria-hidden="true" class="icon_phone main-menu-contact-icon"></span>
														</div>
														<div class="menu-contact-text-container">1-32-312-22, 1-32-112-22</div>
													</li>
													<li class="contact-mail clearfix" >
														<div class="loc-icon-container">
															<span aria-hidden="true" class="icon_mail_alt main-menu-contact-icon"></span>
														</div>
														<div class="menu-contact-text-container">
															<a class="a-mail" href="#">email@felius.com</a>
														</div>
													</li>
												</ul>
											</div>
											
											<div class="col-md-6 menu-map-container hide-max-960 ">
												<!-- Google Maps -->
												<div class="google-map-container">
													<img src="[base_url]theme/theme/images/map-line.png" alt="alt">
												</div>
												<!-- Google Maps / End -->
											</div>
											
										</div>
									</li>
								</ul>
							</li>
								
							
						</ul>
						
					</nav>
				</div>
			</div>
			<!-- END main-menu -->
			
		</div>
		<!-- END container-m-30 -->
		
	</div>
	<!-- END main-menu-container -->
	
	<!-- SEARCH READ DOCUMENTATION -->
	<ul class="cd-header-buttons">
		<li><a class="cd-search-trigger" href="#cd-search"><span></span></a></li>
		</ul> <!-- cd-header-buttons -->
		<div id="cd-search" class="cd-search">
			<form class="form-search" id="searchForm" action="page-search-results.html" method="get">
				<input type="text" value="" name="q" id="q" placeholder="Search...">
			</form>
		</div>
		
	</div>
	<!-- END header-wrapper -->
	
</header>